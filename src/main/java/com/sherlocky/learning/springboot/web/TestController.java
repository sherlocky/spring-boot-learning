package com.sherlocky.learning.springboot.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sherlocky.learning.springboot.entity.User;

@RestController
@RequestMapping("/rest")
public class TestController {
	
   @RequestMapping("/users/{id}")  
    public User getUserById(@PathVariable("id") Long id) {  
        User user = new User();  
        user.setId(id);  
        user.setName("zhang" + id);  
        return user;  
    } 
   
   @RequestMapping("/users")  
   public List<User> getUsers() {
	   List<User> us = new ArrayList<>();
	   for (int i = 0; i < 3; i++) {
		   User user = new User();  
		   user.setId(Long.valueOf(i));
		   user.setName("zhang_" + i);
		   us.add(user);
	   }
       return us;  
   }     
 
//    public static void main(String[] args) {
//        SpringApplication.run(TestController.class, args);
//    }
}
